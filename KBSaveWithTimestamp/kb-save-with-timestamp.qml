import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
// import Qt.labs.folderlistmodel 2.1
import Qt.labs.settings 1.0

import MuseScore 3.0


MuseScore {
	menuPath: "Plugins.KB.Save with Timestamp"
	description: "Save the score as mscz to the chosen directory using workTitle_Timestamp as filename\nTimestamp is in the format YYYYMMDD-HHMMSS"
	version: "1.0"
	pluginType: "dialog"
	requiresScore: true

	width:  360
	height: 80

      property string strExportDirectory: "";
      
	onRun: {
            strExportDirectory = settings.exportDirectory;
            lblExportDirectory.text = strExportDirectory;
            doLog (strExportDirectory);
	    directorySelectDialog.folder = ((Qt.platform.os == "windows")? "file:///" : "file://") + strExportDirectory;
	}

	Component.onDestruction: {
		settings.exportDirectory = strExportDirectory;
	}

	Settings {
		id: settings
		category: "KBSaveScoreWithTimestamp"
		property variant exportDirectory: strExportDirectory
	}

	FileDialog {
		id:           directorySelectDialog
//		title:        qsTranslate("MS::PathListDialog", "Choose the target directory")
		title:        "Choose the target directory"
		selectFolder: true
		visible:      false
		onAccepted: {
			strExportDirectory = this.folder.toString().replace("file://", "").replace(/^\/(.:\/)(.*)$/, "$1$2");
                        doLog (strExportDirectory);
			doSave();
			Qt.quit();
		}
    onRejected: {
        console.log("Canceled")
        Qt.quit()
    }

		Component.onCompleted: visible = false
	}

	GridLayout {
		columns: 2
		anchors.fill: parent
		anchors.margins: 10

		Button {
			id:   selectDirectory
//			text: qsTranslate("PrefsDialogBase", "Select the target-directory ...")
			text: "Select the target-directory ..."
			onClicked: {
doLog ("OnClicked");
				directorySelectDialog.open();
			}
		}
		Button {
			id: lblExportDirectory
			text: strExportdirectory

			onClicked: {
                            doLog ("OnClicked");
			doSave();
			Qt.quit();
			}
		}

	}
	function doSave () {
				var newFileName = curScore.scoreName;
				newFileName = strExportDirectory + "/" + newFileName; 
				var timestamp = (new Date()).toISOString(); //format ISO 8601 YYYY-MM-DDTHH:MM:SS.mmmZ
doLog (timestamp);
				var timestampFilter = /(\d+)\-(\d+)\-(\d+)T(\d+):(\d+):(\d+)\.\d+Z/;
				var timestampReformat = '$1$2$3-$4$5$6';
				newFileName += '_' + timestamp.replace(timestampFilter, timestampReformat);
doLog (newFileName);
				writeScore(curScore, newFileName, 'mscz');
	}

// __________________________________________

	function doLog (strText) {
// __________________________________________

		console.log(strText);
	
	}

}
