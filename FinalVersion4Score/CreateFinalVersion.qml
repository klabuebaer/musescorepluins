import QtQuick 2.9
import MuseScore 3.0
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import Qt.labs.settings 1.0


MuseScore {
    version:  "3.0"
    description: "Create the final version of a score"
    menuPath: "Plugins.CreateFinalVersion"
    requiresScore: true

    Settings {
        id: settings
        category: "CreateFinalVersion"
        property string iniTagFileNameExtension:  "JSON"
        property string iniTagFileName:           "cMetaTags." + iniTagFileNameExtension
        property string iniIncludeFolderPathName: "F:/musicScores/metaData"
        property bool flgShowInfoDialog: true
        property bool flgUseGlobalFolder: false
		property string iniFinalFileNameTemplate: "%composerName% - ;%workTitle%; - %movementTitle%; - %tempoMark%; - %partname%"
    }

    property bool flgShowInfoDialog: true;
    property string txtLog: "";
    property string conPluginName: "MuseScore-Plugin: CreateFinalVersion";

    MessageDialog {
        id: errorDialog
        visible: false
        title: qsTr(conPluginName + " --- Error")
        text: "Error"
        onAccepted: {
            Qt.quit()
        }
        function openErrorDialog(message) {
            text = message
            open()
        }
    }

    MessageDialog {
        id: infoDialog
        visible: false
        title: qsTr(conPluginName + " --- Info")
        text: "Info"
        onAccepted: {
            Qt.quit()
        }
        function openInfoDialog(message) {
            text = message
            open()
        }
    }


    // __________________________________________

    function getNormalizedPath (pstrPath, intIndex) {
        // __________________________________________

//        var lstrPath = pstrPath.replace ("\\", "/");

        var parts = pstrPath.split("/");
        doLog ("parts.length  = " + parts.length );
        if (parts.length <= 1) {
           parts = pstrPath.split("\\");
           doLog ("parts.length  = " + parts.length );
        } 
        var fixedPath = parts[0];
        for (var i = 1; i < parts.length - intIndex; ++i) {
            doLog("part i = " + i + " - " + parts[i]);
            fixedPath += "/" + parts[i];
        }
        if (fixedPath === "") {
            fixedPath = "/";
        }

        doLog ("getNormalizedPath(pathCurScore, intIndex) = " + fixedPath);

        return fixedPath;
    }


    // __________________________________________

    function basename(str) {
        // __________________________________________

        return (str.slice(str.lastIndexOf("/")+1));
    }

    // ______________
    onRun: {
        // ______________

        doLog("hello CreateFinalVersion");

        var score = curScore;
        var flgAbort = false;
        if (!(mscoreMajorVersion == 3 &&
              (mscoreMinorVersion > 4 || mscoreUpdateVersion > 0))) {
            errorDialog.openErrorDialog(qsTr("Minimum MuseScore Version %1 required for " + conPluginName).arg("3.5.1"))
            flgAbort = true;
            Qt.quit()
            Qt.exit(99)
        }
        if (!(curScore)) {
            errorDialog.openErrorDialog(qsTranslate("QMessageBox", "You have no score opened.\nThis plugin requires an opened score to run.\n"))
            flgAbort = true;
            Qt.quit();
        }

        var pathCurScore = curScore.path;
        if (pathCurScore === "") {
            doLog ("Part has no FileInfo. Quit");
            errorDialog.openErrorDialog(qsTranslate("QMessageBox", "Part has no FileInfo. Quitting\n"))
            flgAbort = true;
            Qt.quit();
        }

        if (flgAbort == false) {
            var strNameCurScore = curScore.scoreName;
            doLog("pathCurScore = " + pathCurScore);
            var strPath = getNormalizedPath (pathCurScore, 1);
            var strPath2 = getNormalizedPath (pathCurScore, 2);
            doLog("strPath = " + strPath);
            doLog("strPath2 = " + strPath2);
            var strBaseName = basename(pathCurScore);
            doLog("strBaseName = " + strBaseName);
            doLog("strNameCurScore = " + strNameCurScore);
            
			var excerpts = curScore.excerpts;
			var intNoOfExcerpts = excerpts.length;
			if (intNoOfExcerpts <= 1) {
				strNameCurScore = createTargetFileName (curScore, "", intNoOfExcerpts);
			}
			else {
				strNameCurScore = createTargetFileName (curScore, "Partitur", intNoOfExcerpts);
			}
            
            var strTargetFileNameWOExt = strPath + "/final/" + strNameCurScore;
            var strTargetFileName = strTargetFileNameWOExt + "." + "pdf";
            doLog ("strTargetFileName = " + strTargetFileName);

            doWriteScore (curScore, strTargetFileNameWOExt, "pdf");
		    if (true) {
				doWriteScoreExcerpts (curScore, strPath + "/final/", "pdf");

	//            strTargetFileName = strPath + "/final/" + strNameCurScore + "." + "musicxml";
				doWriteScore (curScore, strTargetFileNameWOExt, "musicxml");

	//            strTargetFileName = strPath + "/final/" + strNameCurScore + "." + "mp3";
				doWriteScore (curScore, strTargetFileNameWOExt, "mp3");
				doWriteScoreExcerpts (curScore, strPath + "/final/", "mp3");

	//            strTargetFileName = strPath + "/final/" + strNameCurScore + "." + "mscz";
				doWriteScore (curScore, strTargetFileNameWOExt, "mscz");
				doWriteScoreExcerpts (curScore, strPath + "/final/", "mscz");

				if (settings.flgUseGlobalFolder) {
					var strTargetFileNameWOExt2 = strPath2 + "/~PDF/" + strNameCurScore;
					doWriteScore (curScore, strTargetFileNameWOExt2, "pdf");
					strTargetFileNameWOExt2 = strPath2 + "/~PDF-TAB/" + strNameCurScore;
					doWriteScoreExcerpts (curScore, strPath2 + "/~PDF-TAB/", "pdf");
					
					strTargetFileNameWOExt2 = strPath2 + "/~MSCZ/" + strNameCurScore;
					doWriteScore (curScore, strTargetFileNameWOExt2, "mscz");
					
					strTargetFileNameWOExt2 = strPath2 + "/~MP3/" + strNameCurScore;
					doWriteScore (curScore, strTargetFileNameWOExt2, "mp3");
					
					strTargetFileNameWOExt2 = strPath2 + "/~MUSICXML/" + strNameCurScore;
					doWriteScore (curScore, strTargetFileNameWOExt2, "musicxml");
				}
			}
            if (flgShowInfoDialog) { 
                infoDialog.openInfoDialog (txtLog);
            }
        }

        Qt.quit();
    }

    function createTargetFileName (pobjScore, pstrPartName, pintExcerptNo) {
        settings.iniFinalFileNameTemplate = "%composerName% - ;%workTitle%; - %movementTitle%; - %tempoMark%; - %partname%";
		var strTemplate = getMetaTag (pobjScore, "FinalFileNameTemplate", "");
		if (strTemplate == "") {
			var strTemplate = settings.iniFinalFileNameTemplate;   
		}
        var templateParts = strTemplate.split(";");
		var strS = "";
		var curPart = "";
		
		for (var ex = 0; ex < templateParts.length; ex++) {

            curPart = templateParts[ex];
            /*
			if (pintExcerptNo > 1) {
				var objExcerpt = pobjScore.excerpts[pintExcerptNo];
				curPart = replacePart(curPart, "%partname%", getMetaTag (objExcerpt.partScore, "partName", ""));
			}
			else {
				curPart = replacePart(curPart, "%partname%", getMetaTag (pobjScore, "partName", ""));
			}
*/	
			curPart = replacePart(curPart, "%partname%", pstrPartName);
			curPart = replacePart(curPart, "%scoreName%", getMetaTag (pobjScore, "scoreName", pobjScore.scoreName));
			curPart = replacePart(curPart, "%movementTitle%", getMetaTag (pobjScore, "movementTitle", ""));
			curPart = replacePart(curPart, "%workTitle%", getMetaTag (pobjScore, "workTitle", ""));
			curPart = replacePart(curPart, "%composerName%", getMetaTag (pobjScore, "composer", ""));
			curPart = replacePart(curPart, "%composer%", getMetaTag (pobjScore, "composer", ""));
			curPart = replacePart(curPart, "%tempoMark%", getMetaTag (pobjScore, "tempoMark", ""));

			strS = strS + curPart;
		}
            
        return strS.trim();

    }

    function replacePart (pstrPart, pstrTag, pstrDefaultValue) {
		if (pstrPart.indexOf(pstrTag) > -1) {
            if (pstrDefaultValue != "") {
           	    pstrPart = pstrPart.replace(pstrTag, pstrDefaultValue);	
            }
            else {
                pstrPart = "";
            }
		}
		return pstrPart;
    }
            
    function getMetaTag (pobjScore, pstrTagName, pstrDefault) {
    var strS = pobjScore.metaTag (pstrTagName);
    if (strS == "" ) {
      strS = pstrDefault;
    }
    return strS;
    }
    
    // ---------------------------------------
    function doWriteScoreExcerpts (pobjScore, pstrTargetPathName, pstrExtension) {
        // ---------------------------------------

        var excerpts = pobjScore.excerpts;
		var intNoOfExcerpts = excerpts.length;
        doLog ("intNoOfExcerpts = " + intNoOfExcerpts);
        for (var ex = 0; ex < intNoOfExcerpts; ex++) {
            var thisScore = excerpts[ex].partScore
            doLog ("thisScore.pathName = " + thisScore.path);
            var partTitle = excerpts[ex].title
			if (intNoOfExcerpts <= 0) {
				partTitle = "";
			}
            var strS = createTargetFileName (pobjScore, partTitle, ex);
            thisScore.source = pstrTargetPathName + strS + "." + pstrExtension;
            doLog ("thisScore.source = " + thisScore.source);
            var res = doWriteScore (thisScore, thisScore.source, pstrExtension);
        }

    }

    // ---------------------------------------
    function createNormalizedFileName(fn) {
        // ---------------------------------------
        fn = fn.trim()
        fn = fn.replace(/ /g,"_")
        fn = fn.replace(/\n/g,"_")
        fn = fn.replace(/[\\\/:\*\?\"<>|]/g,"_")
        return fn
    }

    // ---------------------------------------
    function doWriteScore (pobjScore, pstrTargetFileName, pstrExtension) {
        // ---------------------------------------

        var res = writeScore (pobjScore, pstrTargetFileName, pstrExtension);
        if (!res) {
            doLog (pstrTargetFileName + " : not exported, res = " + res);
            Qt.quit();
            Qt.exit(99);
        }
        else {
            doLog (pstrTargetFileName + " : exported, res = " + res);
            txtLog += "\n" + pstrTargetFileName + " : exported";
        }
        return res;
    }

    // __________________________________________
    function doLog (strText) {
        // __________________________________________

        console.log(strText);
        // txtLog += "\n\r" + strText;

    }

}  // MuseScore
