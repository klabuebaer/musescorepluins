import QtQuick 2.9
import MuseScore 3.0
import QtQuick.Window 2.2
import FileIO 3.0
import Qt.labs.settings 1.0


// https://stackoverflow.com/questions/41900383/parsing-json-in-qml


MuseScore {
      version:  "3.0"
      description: "This plugin creates MetaTags. "
      menuPath: "Plugins.cMetaTagsJSON"
      requiresScore: true

    Settings {
        id: settings
        category: "cMetaTagsJSON"
		property variant iniTagFileNameExtension:  "JSON"
		property variant iniTagFileName:           "cMetaTags." + iniTagFileNameExtension
		property variant iniIncludeFolderPathName: "C:/musicScores/metaData"		
	}
 
     FileIO {id: inFile}
        
//     FileIO {id: outFile}
      
//     FileIO {id: scoreFile}        
      
 //     title:  qstr("This plugin creates MetametaTagNames.")

 property variant arrAllTags: []
 
 // __________________________________________
 
    function setMetaTags (strPathName, intIndex) {
 // __________________________________________
 
	var score = curScore;

	if (intIndex == 0) {
		doLog("includePath = " + strPathName);
		fixedPath = strPathName;
	}
	else {
		var pathCurScore = curScore.path;
		var strNameCurScore = curScore.scoreName;
		doLog("path = " + pathCurScore);
		doLog("basename(pathCurScore) = " + basename(pathCurScore));
		doLog("strNameCurScore = " + strNameCurScore);
		
		var fixedPath = getNormalizedPath(pathCurScore, intIndex);

		if (intIndex == 1) {  // process tag-file(s) for work
		    readTagFile (fixedPath + "/" + strNameCurScore + "." + settings.iniTagFileNameExtension);
		}

		fixedPath = getNormalizedPath(pathCurScore, intIndex) + "/" + settings.iniTagFileName;
	}

	var metaTags = readTagFile (fixedPath);
	return metaTags;
 }

// __________________________________________
 
  function getNormalizedPath (pstrPath, intIndex) {
// __________________________________________  
  
		var parts = pstrPath.split("/");
//		if (parts.length >= 1) {
//			parts = pstrPath.split("\\");  // could be a windows-filename
//		}
		var fixedPath = parts[0];
		for (var i = 1; i < parts.length - intIndex; ++i) {
			doLog("part i = " + i + " - " + parts[i]);
			fixedPath += "/" + parts[i];
		}
		if (fixedPath === "") {
			fixedPath = "/";
		}  

		doLog ("getNormalizedPath(pathCurScore, intIndex) = " + fixedPath);
            
        return fixedPath;      
    }
// __________________________________________

	function readTagFile (pstrTagFileName) {
// __________________________________________

	inFile.source = pstrTagFileName;
	doLog ("about to read file : " + pstrTagFileName);
		
	var metaTagsJSON = inFile.read();
	inFile.close;
      if (metaTagsJSON == "") {
            doLog ("tag-file not found or empty: " + pstrTagFileName);
            
            }
      else {
	doLog("inFile.source = " + inFile.source + "\n" + metaTagsJSON );
	
	var metaTags = "";
	if (metaTagsJSON != "") {
		 metaTags = JSON.parse(metaTagsJSON);

		for (var key in metaTags) {
			var metaTagValue = getTag (metaTags, key);
			if (metaTagValue != "") {
			   if (metaTagValue.startsWith ("/")) {
				  setMetaTags (settings.iniIncludeFolderPathName + metaTagValue, 0);
			   }
			   else {
				  if (getTag (arrAllTags, key) == "") {
					  arrAllTags [key] = metaTagValue;
				  }
			   }
			}
		}
	}
}
	return metaTags;
}	

// __________________________________________

 function getTag (pstrTags, pstrTagName) {
// __________________________________________
 
    var strValue = pstrTags [pstrTagName];
    if (strValue == undefined) {
      strValue = "";
      strValue = curScore.metaTag(pstrTagName);
    }
    else {
      strValue = strValue.trim();
    }
    
	doLog (pstrTagName + ": " + strValue)
    return strValue;
 }
 
// __________________________________________

	function setTagsToScore (pstrTags) {
// __________________________________________
 
         for (var key in pstrTags) {
            var metaTagValue = pstrTags [key];
            
            doLog (key + ": " + metaTagValue)
            if (metaTagValue.trim() != "") {
               curScore.setMetaTag(key, metaTagValue);
            }
        }
        
        var strArranger = getTag (pstrTags, "arranger");
        var strComposer = getTag (pstrTags, "composer");
        var strX = getTag (pstrTags, "opus");
        if (strX != "") {
            strComposer += " - " + qsTr("opus") + " " + strX;
        } 
        if (strArranger != "" & strArranger != strComposer) {
            curScore.addText ("composer", "\n" + strComposer + "\n" + qsTr("Arrang.: ") + strArranger);
            }
        else {
            curScore.addText ("composer", "\n" + strComposer);
        }      
      
        var strDigitizer = getTag (pstrTags, qsTr("ScoreDigitizedBy"));
        if (strDigitizer != "") {
            curScore.addText ("lyricist", "\n" + qsTr("Digitized by: ") + strDigitizer);
        }
        var strT = getTag (pstrTags, "movement_title");
        if (strT == "") {
            var strT = getTag (pstrTags, "movementTitle");
        }
        curScore.setMetaTag ("movementTitle", strT);
        var strS = getTag (pstrTags, "workSection_title");
        if (strS != "") {
            strT = strT + "\n" + strS + "\n";
        }
        curScore.addText ("title", strT);
//	curScore.addText ("subtitle", pstrTags ["workTitle"]);
	curScore.addText ("subtitle", getTag (pstrTags, "workTitle"));
        
      //  curScore.setMetaTag ("no_of_measures", strT);
        curScore.setMetaTag ("duration", curScore.duration);
        curScore.setMetaTag ("keysig", curScore.keysig);

        doLog(pstrTags.workTitle);
        doLog(pstrTags.composer);
 } 
 
 // __________________________________________

 function setMetaTags4Movement (strPathName) {
// __________________________________________
 
	var score = curScore;
	var metaTags = setMetaTags (strPathName, 1);
	var strScoreName = score.scoreName;
	score.setMetaTag ("file_name", strPathName);
      
 }
// __________________________________________

 function setMetaTags4Work (strPathName) {
// __________________________________________

  var metaTags = setMetaTags (strPathName, 2);
  var p = setMetaTags4Composer(strPathName);
 }
 // __________________________________________

  function setMetaTags4Composer (strPathName) {
// __________________________________________

      return setMetaTags (strPathName, 3);
 }

// __________________________________________

 function basename(str) {
// __________________________________________

    return (str.slice(str.lastIndexOf("/")+1));
}
 
      onRun: {
		doLog("hello cMetaTagsJSON");
		var score = curScore;

        var pathCurScore = curScore.path;
        var strNameCurScore = curScore.scoreName;
        doLog("path = " + pathCurScore);
        doLog(basename(pathCurScore));
        doLog(strNameCurScore);
		var p = "";        
		p = setMetaTags4Movement(pathCurScore);
		p = setMetaTags4Work(pathCurScore);
			
		p = setTagsToScore (arrAllTags);
        
		Qt.quit();
		}

// __________________________________________

	function doLog (strText) {
// __________________________________________

		console.log(strText);
	
	}
	
  }  // MuseScore
