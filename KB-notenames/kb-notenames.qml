//=============================================================================
//  MuseScore
//  Music Composition & Notation
//
//  Note Names Plugin
//
//  Copyright (C) 2012 Werner Schweer
//  Copyright (C) 2013, 2014 Joachim Schmitz
//  Copyright (C) 2014 Jörn Eichler
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License version 2
//  as published by the Free Software Foundation and appearing in
//  the file LICENCE.GPL
//=============================================================================

import QtQuick 2.9
import MuseScore 3.0

MuseScore {
   version: "0.5"
   description: qsTr("KBNoteNames by KlaBueBaer")       
   menuPath: "Plugins.NoteNames." + qsTr("KB Note Names") 

   property variant noteNames: [ "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]

    property variant 
      tpcText : [
               "F♭♭", // -1
             "C♭♭",   // 0
             "G♭♭", 
             "D♭♭", 
             "A♭♭", 
             "E♭♭", 
             "B♭♭", 
             "F♭", 
             "C♭", 
             "G♭", 
             "D♭", 
            "A♭", 
            "E♭", 
            "B♭", 
            "F", 
            "C", 
            "G", 
            "D", 
            "A", 
            "E", 
            "B", 
            "F♯", 
            "C♯", 
            "G♯", 
            "D♯", 
            "A♯", 
            "E♯", 
            "B♯", 
//            "F" + "\u2081", 
            "F♯♯", 
            "C♯♯", 
            "G♯♯", 
            "D♯♯", 
            "A♯♯", 
            "E♯♯", 
            "B♯♯"
   ];
   property variant octaveNumbers : [
   "\u2080","\u2081","\u2082","\u2083","\u2084","\u2085","\u2086","\u2087","\u2088","\u2089" 
   ];
   property variant octaveSymbols : [
	",,",
 ",", 
" ", 
" ", 
"\'", 
"\'\'", 
"\'\'\'", 
"\'\'\'\'", 
"\'\'\'\'\'", 
"\'\'\'\'", 
"\'\'\'\'\'\'"
   ]

         property variant octaveColors : [ // "#rrggbb" with rr, gg, and bb being the hex values for red, green, and blue, respectively
               "#e21c48", // C''
               "#f26622", // C'
               "#aa5500", // C
               "#aa00ff", // c
               "#000000", // c'
               "#00aa00", // c''
               "#00ff00", // c'''
               "#009c95", // c''''
               "#0071bb", // c'''''
               "#5e50a1", // c''''''
               "#8d5ba6", // 
               "#cf3e96"  // 
               ]
      property string black : "#000000"
   
   property variant flgShowOctaveSymbol : false
   property variant flgShowOctaveNumber : true
   property variant flgArrangeNamesVertical : false
   property variant lastUsedNoteName : " "
   property variant flgDoColorOctaves : true
   property variant flgDoColorNoteNames : true
   property variant flgCreateNamesInChordLow2High : true

    function getOctaveNumber(pitch) {
        return Math.floor((pitch - 12) / 12)  // Though the first valid midi note is A0, C0 whould have the number 12
    }

    function getNoteName(pitch) {
        var index = (pitch - 12) % 12
        return noteNames[index]
    }
    
    function getFullNoteName(pitch) {
      return getNoteName(pitch) + getOctaveNumber(pitch)
    }
    
   function createNoteNames (cursor, voice, notes) {
                var itext  = newElement (Element.STAFF_TEXT);
                itext.subStyle = Tid.USER10;
//                var itext  = newElement (Element.FINGERING);
                itext.placement = Placement.ABOVE;    // BELOW
                itext.autoplace = true;
                
                nameChord (notes, itext);

//                switch (voice) {
//                case -1: break;
//                  case 0: itext.pos.y =  -1.5; break;
//                  case 1: itext.pos.y = 10; break;
//                  case 2: itext.pos.y = -1; break;
//                  case 3: itext.pos.y = 12; break;
//                }
              
//                if ((voice == 0) && (notes[0].pitch > 83))
//                  itext.pos.x = 1;
                if (lastUsedNoteName != itext.text) {
                    cursor.add (itext);
                }
                lastUsedNoteName = itext.text;
   }

   function nameChord (notes, text) {
      var sep = "-"; 
      if (flgArrangeNamesVertical) {
            sep ="\n";
      }
      
      for (var i = 0; i < notes.length; i++) {
         if ( i > 0 )
            if (flgCreateNamesInChordLow2High) {
               text.text += sep;
            }
            else {
               text.text = sep + text.text;
            }

            if (typeof notes[i].tpc === "undefined") // 
                 return
			
            var currentNote = notes [i]
            var currTonalPitchClass = currentNote.tpc

		 var currNoteName = "";
		 if (currTonalPitchClass >= -1 && currTonalPitchClass <= 33) {
			currNoteName = qsTr(tpcText[currTonalPitchClass + 1]);
		 }
		 else {
			currNoteName = qsTr("?");
		 }
			
		var octaveNumb = getOctaveNumber(currentNote.pitch);

		if (flgShowOctaveNumber) {
			currNoteName += octaveNumbers[octaveNumb];
		}
		if (flgShowOctaveSymbol) {
			if (octaveNumb < 3) {
			   currNoteName = octaveSymbols[octaveNumb] + currNoteName;
			}
			else {
			   currNoteName += octaveSymbols[octaveNumb];
			}
		}
		
		if (octaveNumb > 2) {
			currNoteName = currNoteName.toLowerCase();
		}
        else {
			currNoteName = currNoteName.toUpperCase();
        }
		
//		if (currNoteName != lastUsedNoteName) {
            console.log (currNoteName + " -> " + lastUsedNoteName)
            if (flgCreateNamesInChordLow2High) {
			   text.text += currNoteName;
            }
            else {
			   text.text = currNoteName + text.text;
            }
//		}

		if (flgDoColorOctaves) {
                        currentNote.color = octaveColors[octaveNumb];
                }
                
		if (flgDoColorNoteNames) {
                        text.color = octaveColors[octaveNumb];
                }
  
// change below false to true for courtesy- and microtonal accidentals
// you might need to come up with suitable translations
// only #, b, natural and possibly also ## seem to be available in UNICODE

         if (false) {
            switch (notes[i].userAccidental) {
               case  0: break;
               case  1: text.text = qsTr("#") + text.text; break;
               case  2: text.text = qsTr("b") + text.text; break;
               case  3: text.text = qsTr("##") + text.text; break;
               case  4: text.text = qsTr("bb") + text.text; break;
               case  5: text.text = qsTr("natural") + text.text; break;
               case  6: text.text = qsTr("flat-slash") + text.text; break;
               case  7: text.text = qsTr("flat-slash2") + text.text; break;
               case  8: text.text = qsTr("mirrored-flat2") + text.text; break;
               case  9: text.text = qsTr("mirrored-flat") + text.text; break;
               case 10: text.text = qsTr("mirrored-flat-slash") + text.text; break;
               case 11: text.text = qsTr("flat-flat-slash") + text.text; break;
               case 12: text.text = qsTr("sharp-slash") + text.text; break;
               case 13: text.text = qsTr("sharp-slash2") + text.text; break;
               case 14: text.text = qsTr("sharp-slash3") + text.text; break;
               case 15: text.text = qsTr("sharp-slash4") + text.text; break;
               case 16: text.text = qsTr("sharp arrow up") + text.text; break;
               case 17: text.text = qsTr("sharp arrow down") + text.text; break;
               case 18: text.text = qsTr("sharp arrow both") + text.text; break;
               case 19: text.text = qsTr("flat arrow up") + text.text; break;
               case 20: text.text = qsTr("flat arrow down") + text.text; break;
               case 21: text.text = qsTr("flat arrow both") + text.text; break;
               case 22: text.text = qsTr("natural arrow down") + text.text; break;
               case 23: text.text = qsTr("natural arrow up") + text.text; break;
               case 24: text.text = qsTr("natural arrow both") + text.text; break;
               case 25: text.text = qsTr("sori") + text.text; break;
               case 26: text.text = qsTr("koron") + text.text; break;
               default: text.text = qsTr("?") + text.text; break;
            } // end switch userAccidental
         } // end if courtesy- and microtonal accidentals
      } // end    for (var i = 0; i < notes.length; i++)
      
   }


   onRun: {
      if (typeof curScore === 'undefined')
        Qt.quit();
		
      var cursor = curScore.newCursor();
      var p = curScore.path;
      console.log (p);
      
      var startStaff;
      var endStaff;
      var endTick;
      var fullScore = false;

      cursor.rewind(1);

      if (!cursor.segment) { // no selection
        fullScore = true;
        startStaff = 0; // start with 1st staff
        endStaff  = curScore.nstaves - 1; // and end with last
      } 
      else {
        startStaff = cursor.staffIdx;
        cursor.rewind(2);
        if (cursor.tick == 0) {
          endTick = curScore.lastSegment.tick + 1;
        } 
        else {
          endTick = cursor.tick;
        }
        endStaff   = cursor.staffIdx;
      }
      console.log(startStaff + " - " + endStaff + " - " + endTick)

      for (var staff = startStaff; staff <= endStaff; staff++) {
        for (var voice = 0; voice < 4; voice++) {
          cursor.rewind(1); // beginning of selection
          cursor.voice    = voice;
          cursor.staffIdx = staff;

          if (fullScore)  // no selection
            cursor.rewind(0); // beginning of score

          while (cursor.segment && (fullScore || cursor.tick < endTick)) {
            if (cursor.element && cursor.element.type == Element.CHORD) {

               var graceChords = cursor.element.graceNotes;
               for (var i = 0; i < graceChords.length; i++) {  // iterate through all grace notes
                 createNoteNames (cursor, voice, graceChords[i].notes);
               }

              createNoteNames (cursor, voice, cursor.element.notes);
            
            } // end if CHORD
            cursor.next();
            
          } // end while segment
        } // end for voice
      } // end for staff
      Qt.quit();
   } // end onRun
}
